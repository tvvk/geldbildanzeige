#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <string>

#include "config.h"

const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
String url;

struct Geldschein {
  const int amount;
  const String currency;
};

Geldschein geldscheine[] = {
  {10, "PLN"},
  {20, "SEK"},
  {50, "NOK"},
  {5, "GBP"},
  {2000, "HUF"},
  {10, "RON"},
  {1, "USD"},
  {2, "BRL"},
  {20, "BOB"},
  {10, "PEN"},
  {1000, "KES"},
  {10000, "TZS"},
  {5, "AED"},
  {1, "JOD"},
  {100000, "VND"},
  {20, "ILS"},
};

void setup() 
{
  Serial.begin(115200);
  url = buildUrlForAllCurrencies();
}

void loop() 
{

    // Wifi connect
    Serial.println(String("Connecting to ") + WIFI_SSID + String("..."));
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) 
    {
      Serial.println(String("Waiting for connection... ") + WiFi.status());

      delay(1000);
    }

    // Make api request
    Serial.print("Requesting currencies from ");    
    Serial.println(url);    
    
    HTTPClient http; 
    http.begin(url);
    int httpCode = http.GET();

    if (httpCode == 200) 
    {
      Serial.println("Request was successfull");    
      DynamicJsonBuffer jsonBuffer(bufferSize);
      JsonObject& root = jsonBuffer.parseObject(http.getString());
      double amount = calculateAmountInEuro(root);     
    } else {
      Serial.println("Request was not successfull, Status code was: " + httpCode);    
    }
    
    Serial.println("Disconnecting");
    http.end(); 
    WiFi.disconnect(true, false);

    delay(60 * 1000);
}

double calculateAmountInEuro(JsonObject& root) {
    double total = 0;
    for (Geldschein& geldschein : geldscheine) {
      double exchangeRate = root["rates"][geldschein.currency];
      double amount = geldschein.amount / exchangeRate;
      Serial.println("Exchange rate for " + geldschein.currency + " is " + exchangeRate + " - " + geldschein.amount + " " + geldschein.currency + " is " + amount + " EUR");
      total += amount;
    }
    Serial.print("Total amount calculated: ");
    Serial.println(total);
    return total;  
}

String buildUrlForAllCurrencies() {
    String url = "http://data.fixer.io/api/latest?access_key=" + FIXER_API_KEY + "&format=1&symbols=";
    for (Geldschein& geldschein : geldscheine) {
      url += geldschein.currency + ",";
    }
    return url;
}

